# 1 create rspec necessary files (run once per app)
# rails generate rspec:install 

# 2 create a file to write tests in (one for each model/controler/view)
# rails generate rspec:model user 

# 3 if have seed for test:
# rake db:test:load 

# 4 run tests, from terminal
# rspec 

require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { User.new }
  let(:user1) { User.create(name: 'user one') }
  let(:user2) { User.create(name: 'user two') }
  let(:group1) { user1.created_groups.create(name: 'group_1 of user_1') }
  let(:group2) { user1.created_groups.create(name: 'group_2 of user_1') }
  let(:group3) { user2.created_groups.create(name: 'group_1 of user_2') }

  it 'correct setup' do
    expect(user).not_to be_nil
  end

  it 'created' do
    # reusable objects from outside
    user1

    # tested association goes inside expect
    expect(User.first.name).to eql(user1.name)
  end

  it 'create a group' do
    user1
    group1

    expect(user1.created_groups.first.name).to eql(group1.name)
  end

  it 'see own groups created' do
    user1
    group1
    group2

    expect(user1.created_groups.first.creator_id).to eql(group1.creator_id)
    expect(user1.created_groups.second.creator_id).to eql(group2.creator_id)
  end

  it 'can join other people group' do
    user1
    user2
    group3
    
    user1.membership.create(group_id: 3)

    expect(group3.).to eql('ok')
  end

  # it 'see all groups joined' do
  # end
end
