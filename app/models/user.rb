class User < ApplicationRecord
    has_many :created_groups, foreign_key: :creator_id, class_name: "Group"

    has_many :membership
    has_many :groups, through: :membership
end
